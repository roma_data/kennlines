<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mandalikaws' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'DaczDN?613V4oo4wl7K2LBTo4!`fFD))^{e^Cbq+ExQU/!({eB:3{dKJ_i-RgRT>' );
define( 'SECURE_AUTH_KEY',  'j*`w]+q45t %KOM6Kf[)w%j?}/JizG+|])3v*Mpki`SHCMt-]@YM+G*{zQWl&N4O' );
define( 'LOGGED_IN_KEY',    '_it*]qSCH;6,Le8d+j$h|$!kf)UsB19.Qf-*{GC-PfW5YpS}D:Y& +.#O )~A;7y' );
define( 'NONCE_KEY',        '3r^4%xY|zYRKV)_w#[9T*lptLz5yi69 jj-OIs2p){SsQ6j/Dd.CApMjcruTtp%U' );
define( 'AUTH_SALT',        '5t}!~pHfzacHpIIPt_yJ.(DgV[@P6}9:nbfTvSwK3V<zHd$^&3tAy.UD|sBjp~F|' );
define( 'SECURE_AUTH_SALT', '/td5pS5`G0TRvbqM$F=4S&,&Nf=~ BT%9O1$j1v9 }[olUO{vfu64<Aw~=yOb,Mv' );
define( 'LOGGED_IN_SALT',   'bHn7m~fD!lx V)=.CzL?[$d2Beupi*|G%jRZyo-QwuN]h{y4s+w:$.Wu%l2WA~J#' );
define( 'NONCE_SALT',       '$Vt6~r]IELwt(3b4j& 8AQ2S-p.j,5QQ-s(<uCdr2Dt5&3lcWf4tjVZZ9x/Xtq;h' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
