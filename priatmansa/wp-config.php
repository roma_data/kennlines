<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'priatmansa' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'a<0 q_VvC{Li<b.8RGGk7X%~h3w_$Dt;MML/<)&$m7mt]%mbV3rOcun_>+e6jTn4' );
define( 'SECURE_AUTH_KEY',  'JUy/ 2v3OvL!ohOPCPW8VIm:YU[3`GyYS!,`[AKEcGBa ZJ-{$P:W81y!W}|cKKe' );
define( 'LOGGED_IN_KEY',    'Y`17S@-4x.l*@d_H#g(lruqI~J.5G`09C<!.!<6/eg1M7ftw/(!<,f~x .lN8hra' );
define( 'NONCE_KEY',        '.XRYS]Z,H8yniSok5Rh`Xs*-bz0Uh+?5-{WJOUMMrp+Pt+J[%4`B^3p%5Zg&qA8j' );
define( 'AUTH_SALT',        'tibhjD;vET4Ed:D@{~Z--b~=@_fN}F%|G9jA@uGJ/Y;5Q},KRQisO&6^dWyZx]yW' );
define( 'SECURE_AUTH_SALT', 'bqdI%arbAc1e1~^8/Ab5C0|%)&=7(Y0Cwmd||[lEB3-*AAv=Dbj>uI:-xuW88Hrh' );
define( 'LOGGED_IN_SALT',   'g[l9YsXt-b^;}~=0Plcq*xDSh]_tWfwXuIW!39U.WWbDPCaJkWRv@=;Mbtz(+VNT' );
define( 'NONCE_SALT',       '$.ZS6<l)E-`;26fwW=@{#->.F[i NTK|&HEl.<z$1wTm7Wu*lpzq2[jh5a*>hFt<' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
