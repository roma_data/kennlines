<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'kcgroup' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'CT8n*/c`evF;eggCUXLWt3x($r=d#/idz{DJD_ k`+.g0B4~0|s6L[j_=mB3f(IO' );
define( 'SECURE_AUTH_KEY',  '$5s>N7tTygHc1~,Q.*`ts$uMv_ScK|%99el2r<F5IpFBg, tSiuyb9/Co;]&FC/X' );
define( 'LOGGED_IN_KEY',    'R.A/Diy0>SjOi=JNCRA3aIyj( FQ|vey+9,ffHBuI:Bl+@nx[@Ir0gkav,7sLL]#' );
define( 'NONCE_KEY',        'pxND@{:A?gO3In*]XChY&J;*Of.K]Z=L!5HG)ooR*Kb)E(_#Xo@x?y%H!@0/mD28' );
define( 'AUTH_SALT',        ':g@aEu0R~vpmD<Ub^]F^J*8Ps~`hGr,reLjRbYKJI:;z6J2E7EED(;O1N$+C6J]L' );
define( 'SECURE_AUTH_SALT', 'jVa.uqLTI<3|tr=_F`.v[ah1+[R>$MsM(Y+az!Hu8|cX !`rCIEI(a8P_p#=PfOY' );
define( 'LOGGED_IN_SALT',   '5xVP T9wFrUlsM+T7mJ0{]2bQ,12+q39<}:G @Q4x7`~]Gm815?+{Lw^lz5-3G{d' );
define( 'NONCE_SALT',       'MRcvb%h-M?qSz[b/0=&$tiif]W_Foh Y*eAk_2of~8O8.B*ULHJ{0m4^VS(9+#O5' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );



define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', '192.168.64.2');
define('PATH_CURRENT_SITE', '/kcgroup/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);



/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
